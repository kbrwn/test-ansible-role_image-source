FROM centos:7

MAINTAINER duck@redhat.com <Marc Dequènes (Duck)>

# set PATH and use pip install --user because on Red Hat based system it installs in system paths and not /usr/local
# which then sometimes conflicts when using the equivalent RPM
ENV PATH ~/.local/bin:$PATH

# debug
RUN id
RUN export

# EPEL needed
RUN yum -y install epel-release
# this image is very bare and we need cron installed to test this role
RUN yum -y install cronie crontabs
# needed for Shippable
RUN yum -y install which openssh-clients git
# the Ansible package version is far too old (1.5.4)
RUN yum -y install python-pip python-devel libffi-devel openssl-devel make gcc gcc-c++ screen
RUN rpm -ql python-pip
RUN pip install --user ansible
# TEMP: fix a bug using the cron module affecting stable
RUN pip install --user --upgrade http://releases.ansible.com/ansible/ansible-2.2.0.0-0.2.rc2.tar.gz
# check the installed version
RUN ansible --version
# ansible-lint is not packaged
RUN pip install --user ansible-lint
RUN pip install --user flake8

COPY files/run_tests.sh /usr/local/bin/run_tests

