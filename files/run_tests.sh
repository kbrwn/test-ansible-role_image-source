#!/bin/bash

# no need to go on if a step fails
set -e
# log what we do to help debug
set -x


TYPE=$1

case $TYPE in
  setup)
    # setup the environment to run ansible
    sed -i "s/<ROLE_NAME>/${SHIPPABLE_BUILD_DIR/*\//}/g" role_test.yml
    echo -e "[defaults]\nroles_path = ../\n" > ansible.cfg
    ;;

  preflight)
    # verify the syntax of the playbook
    ansible-lint tasks/main.yml
    # verify python script with pyflakes and pep8
    set -e ; for i in files/*.py; do flake8 $i ;done
    ;;

  deploy)
    # run a syntax check
    ansible-playbook -i '127.0.0.1,' --syntax-check role_test.yml
    # check that the role can be run
    ansible-playbook -i '127.0.0.1,' -c local -vvvv role_test.yml
    # verify idempotence ( ie, running a 2nd time shouldn't change anything )
    ansible-playbook -i '127.0.0.1,' -c local -vvvv role_test.yml | grep -q 'changed=0.*failed=0'
    ;;
esac

